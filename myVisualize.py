import pymysql
import pyecharts.options as opts
from pyecharts.charts import Line, Pie,Radar
from config import *

def create_temp():
    db = pymysql.connect(host=HOST, user=USER, passwd=PASSWORD, db=DB, charset=CHARSET)
    cursor = db.cursor()
    cursor.execute('SELECT * FROM weather_spider;')
    data = cursor.fetchall()
    max_temp_list = []
    min_temp_list = []
    day_list = []
    for d in data:
        max_temp_list.append(d[3].split('/')[0].replace('℃', ''))
        min_temp_list.append(d[3].split('/')[1].replace('℃', ''))
        day_list.append(d[0][:11])
    line = Line()
    line.add_xaxis(day_list)
    line.add_yaxis(series_name="最高气温", y_axis=max_temp_list, is_symbol_show = False, 
        markpoint_opts=opts.MarkPointOpts(
            data=[
                opts.MarkPointItem(type_="max", name="最大值"),
                opts.MarkPointItem(type_="min", name="最小值"),
            ]
        ),
        markline_opts=opts.MarkLineOpts(
            data=[opts.MarkLineItem(type_="average", name="平均值")]
        ))
    line.add_yaxis(series_name="最低气温", y_axis=min_temp_list, is_symbol_show = False, 
        markpoint_opts=opts.MarkPointOpts(
            data=[
                opts.MarkPointItem(type_="max", name="最大值"),
                opts.MarkPointItem(type_="min", name="最小值"),
            ]
        ),
        markline_opts=opts.MarkLineOpts(
            data=[opts.MarkLineItem(type_="average", name="平均值")]
        ))
    line.set_global_opts(yaxis_opts=opts.AxisOpts(name="温度（℃）"), 
        title_opts=opts.TitleOpts(title="南昌气温变化表"), 
        tooltip_opts=opts.TooltipOpts(trigger="axis"))
    
    line.render('南昌气温变化表.html')
    print('气温图生成成功')
    db.close()
    cursor.close()

def create_weather():
    db = pymysql.connect(host=HOST, user=USER, passwd=PASSWORD, db=DB, charset=CHARSET)
    cursor = db.cursor()
    attr = ["雨", "多云", "晴", "阴", "雪", "雾", "霾"]
    rain = cursor.execute('SELECT * FROM weather_spider WHERE weather_type like "%雨%";')
    cloud = cursor.execute('SELECT * FROM weather_spider WHERE weather_type like "%多云%";')
    sun = cursor.execute('SELECT * FROM weather_spider WHERE weather_type like "%晴%";')
    overcast = cursor.execute('SELECT * FROM weather_spider WHERE weather_type like "%阴%";')
    snow = cursor.execute('SELECT * FROM weather_spider WHERE weather_type like "%雪%";')
    fog = cursor.execute('SELECT * FROM weather_spider WHERE weather_type like "%雾%";')
    smog = cursor.execute('SELECT * FROM weather_spider WHERE weather_type like "%霾%";')
    weather = [rain, cloud, sun, overcast, snow, fog, smog]
    pie = (
        Pie()
        .add("", [list(z) for z in zip(attr, [rain, cloud, sun, overcast, snow, fog, smog])])
        .set_global_opts(title_opts=opts.TitleOpts(title="天气占比表"))
        .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}"))
    )
    
    pie.render('南昌天气占比表.html')
    print('天气图生成成功')
    db.close()
    cursor.close()

# 风向雷达图
def create_wind():
    db = pymysql.connect(host=HOST, user=USER, passwd=PASSWORD, db=DB, charset=CHARSET)
    cursor = db.cursor()
    # 获取风向数据分别有8个风向
    north = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "北风%";')
    west_north = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "西北风%";')
    west = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "西风%";')
    west_south = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "西南风%";')
    south = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "南风%";')
    east_south = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "东南风%";')
    east = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "东风%";')
    east_north = cursor.execute('SELECT * FROM weather_spider WHERE  wind_power like "东北风%";')
    wind_list = [north,west_north,west,west_south,south,east_south,east,east_north]
    v1 = [wind_list]
    c = (
        Radar()
        .add_schema(
            schema=[
                opts.RadarIndicatorItem(name="北风",max_=200),
                opts.RadarIndicatorItem(name="西北风",max_=10),
                opts.RadarIndicatorItem(name="西风",max_=10),
                opts.RadarIndicatorItem(name="西南风",max_=100),
                opts.RadarIndicatorItem(name="南风",max_=30),
                opts.RadarIndicatorItem(name="东南风",max_=30),
                opts.RadarIndicatorItem(name="东风",max_=30),
                opts.RadarIndicatorItem(name="东北风",max_=150),
            ]
        )
        .add("风向", v1)
        .set_series_opts(label_opts=opts.LabelOpts(is_show=True))
        .set_global_opts(
            title_opts=opts.TitleOpts(title="风向图"), legend_opts=opts.LegendOpts()
        )
        .render("南昌风向图.html")
    )
    print('风向图生成成功')

if __name__ == '__main__':
    create_temp()
    create_weather()
    create_wind()